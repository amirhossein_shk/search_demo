import React from "react";

interface AppContextProps {
    favorites: any;
    addItemToFavorites(param: object): void;
    removeItemFromFavorites(param: object): void;
}

export default React.createContext<AppContextProps>({
    favorites: [],
    addItemToFavorites: () => {},
    removeItemFromFavorites: () => {}
});