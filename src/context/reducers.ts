export const ADD_ITEM_TO_FAVORITES = "ADD_ITEM_TO_FAVORITES";
export const REMOVE_ITEM_FROM_FAVORITES = "REMOVE_ITEM_FROM_FAVORITES";

const addItemToFavorites = (item, state) => {
    const updatedFavorites = [...state.favorites, item];
    return { ...state, favorites: updatedFavorites };
}

const removeItemFromFavorites = (itemId, state) => {
    const favorites = state.favorites.filter(item => item.identifier !== itemId);
    return {...state, favorites};
}

export const appReducer = (state, action) => {
    switch (action.type) {
        case ADD_ITEM_TO_FAVORITES:
            return addItemToFavorites(action.payload, state);
        case REMOVE_ITEM_FROM_FAVORITES:
            return removeItemFromFavorites(action.itemId, state);
        default:
            return state;
    }
}