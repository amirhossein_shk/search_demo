import React from 'react';
import { Router } from 'react-router-dom';
import { createBrowserHistory } from "history";
import Routes from "./routes";
import GlobalState from "./context/GlobalState";

const history = createBrowserHistory();
function App() {

  return (
      <GlobalState>
        <Router history={history}>
          <Routes />
        </Router>
      </GlobalState>
  );
}

export default App;