import React from "react";
import { Route } from "react-router-dom";

import DefaultLayout from "../pages/_layouts/default";

interface routesProps {
    component: React.ElementType;
    path: string;
    exact?: boolean;
}

const RouteWrapper: React.FC<routesProps> = ({ component: Component, ...rest }) => {

    const Layout = DefaultLayout;

    return (
        <Route
            {...rest}
            render={props => (
                <Layout>
                    <React.Suspense fallback={<span>Loading...</span>}>
                        <Component {...props} />
                    </React.Suspense>
                </Layout>
            )}
        />
    );
}

export default RouteWrapper;