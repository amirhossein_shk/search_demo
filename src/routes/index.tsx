import React from "react";
import { Switch } from "react-router-dom";
import Route from "./Route";
const Search = React.lazy(() => import("../pages/Search"));
const Home = React.lazy(() => import("../pages/Home"));

const Routes: React.FC = () => {
    return (
        <Switch>
            <Route path="/" component={ Home } exact />
            <Route path="/search" component={ Search } />
        </Switch>
    );
}

export default Routes;