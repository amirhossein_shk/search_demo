import React, { useContext, useEffect, useRef, useState } from "react";
import { Box, Grid, Typography } from "@mui/material";
import Input from "../../components/Input";
import Button from "../../components/Button";
import { useFetch } from "../../hooks/useFetch.hook";
import SearchTable from "./SearchTable";
import FavoriteButton from "../../components/FavoriteButton/FavoriteButton";
import appContext from "../../context/appContext";

interface searchData {
    identifier: string;
    title: string;
}

const Search: React.FC = () => {

    const [searchData, setSearchData] = useState<searchData[]>([]);
    const [searchText, setSearchTex] = useState('');
    const [query, setQuery] = useState('');
    const [userClicked, setUserClicked] = useState(true);
    const [pageCounter, setPageCounter] = useState(1);

    const inputRef = useRef<HTMLInputElement | null>();

    const { status, data } = useFetch(query);

    const context = useContext(appContext);

    const handleChange = (e: React.FormEvent<HTMLInputElement>) => {
        setSearchTex(e.currentTarget.value);
    }

    const handleDoSearch = (userClicked = false) => {
        setUserClicked(userClicked);
        let query = `https://archive.org/advancedsearch.php?q=${searchText}&rows=50&page=1&output=json`;
        if (userClicked) {
            setSearchData([]);
        } else if (!userClicked) {
            query = `https://archive.org/advancedsearch.php?q=${searchText}&rows=50&page=${pageCounter}&output=json`;
        }
        setQuery(query);
    }

    const handleOnClickFavorite = (data, value) => {
        if (value) {
            context.addItemToFavorites(data);
        } else {
            context.removeItemFromFavorites(data.identifier);
        }
    }

    useEffect(() => {
        if (status === 'fetched' && data && data.response.docs.length) {
            const mappedData = data.response.docs.map(item => ({
                identifier: item.identifier,
                title: item.title,
                downloads: item.downloads,
                creator: item.creator,
                mediatype: item.mediatype,
                favorite: <FavoriteButton
                    onClick={(value) => handleOnClickFavorite(item, value)}
                    value={context.favorites.find(i => i.identifier === item.identifier) && 1}
                />
            }));
            setSearchData([...searchData, ...mappedData]);
            setPageCounter(pageCounter + 1);
        }
    }, [data]);

    const searchButton = (
        <Box width={200} ml={1}>
            <Button
                {...{
                    variant: 'contained',
                    size: 'medium',
                    onClick: () => handleDoSearch(true),
                    color: 'secondary',
                    disabled: (status === 'fetching' || searchText === '') && userClicked,
                    fullWidth: true
                }}
            >
                { status === 'fetching' && userClicked ? 'Processing' : "Search" }
            </Button>
        </Box>
    );

    const handleFocusOnInput = () => {
        inputRef.current && inputRef.current.focus();
    }

    return (
        <Grid container
              direction="row"
              spacing={2}
              justifyContent={"center"}
        >
            <Grid item
                  xs={12}
                  display={"flex"}
                  alignItems={"center"}
                  justifyContent={"center"}
                  mt={6}
            >
                <Box width={450}>
                    <Input
                        {...{
                            onChange: handleChange,
                            id: 'search',
                            label: 'Search your keyword',
                            defaultValue: '',
                            disabled: false,
                            type: 'search',
                            fullWidth: true,
                            InputProps: {
                                endAdornment: searchButton,
                                inputRef: inputRef
                            }
                        }}
                    />
                </Box>
            </Grid>
            <>
                {
                    searchData.length ?
                        <SearchTable
                            status={status}
                            searchData={searchData}
                            handleDoSearch={handleDoSearch}
                        /> :
                        (
                            status === "fetching" ?
                                <Typography textAlign={"center"} mt={12}>
                                    Please wait...
                                </Typography> :
                                <Typography textAlign={"center"} mt={12}>
                                    Search results will be displayed here...
                                    <br/>
                                    <Button
                                        {...{
                                            onClick: handleFocusOnInput
                                        }}
                                    >Search Now</Button>
                                </Typography>
                        )
                }
            </>
        </Grid>
    )
}

export default Search;