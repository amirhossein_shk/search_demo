import React, {useEffect} from "react";
import Rating from '@mui/material/Rating';
import StarIcon from '@mui/icons-material/Star';

interface favoriteButtonProps {
    onClick(param: number): void;
    value: number;
}

const FavoriteButton:React.FC<favoriteButtonProps> = (props) => {
    const [value, setValue] = React.useState<number | null>(0);

    useEffect(() => {
        props.value && setValue(props.value);
    }, []);

    const handleOnChange = (newValue) => {
        setValue(newValue);
        props.onClick(newValue);
    }

    return (
        <Rating
            name="hover-feedback"
            value={value}
            max={1}
            onChange={(event, newValue) => {
                handleOnChange(newValue);
            }}
            emptyIcon={<StarIcon style={{ opacity: 0.55 }} fontSize="inherit" />}
        />
    )
}

export default FavoriteButton;