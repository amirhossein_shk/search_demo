import React from "react";
import { AppBar, Toolbar, Typography, Button } from "@mui/material";
import styles from "./styles/Header.module.scss";
import { Link as RouterLink } from "react-router-dom";

interface headerProps {
    logoTitle: string,
    menuItems: { id: number, title: string, href: string }[]
}

const Header: React.FC<headerProps> = ({ logoTitle, menuItems }) => {
    const logo = (
        <Typography className={styles.logo} variant="h6" component="h1">
            { logoTitle || "Logo Goes here" }
        </Typography>
    );

    const getMenuButtons = () => {
        return menuItems.map(({ id, title, href }) => {
            return (
                <Button
                    variant={"outlined"}
                    color={"inherit"}
                    className={styles.btn}
                    key={id}
                    to={href}
                    component={RouterLink}
                >
                    { title }
                </Button>
            );
        });
    };

    const displayDesktop = () => {
        return <Toolbar className={styles.toolbar}>
            { logo }
            <div> { getMenuButtons() } </div>
        </Toolbar>;
    };

    return (
        <AppBar className={styles.Header}>{displayDesktop()}</AppBar>
    );
}

export default Header;