import React from "react";
import Button from '@mui/material/Button';

const Btn: React.FC = ({ children, ...args }) => {
  return (
    <Button {...args}>
      { children }
    </Button>
  )
}

export default Btn;